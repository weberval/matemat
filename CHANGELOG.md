# Matemat Changelog

<!-- BEGIN RELEASE v0.3.5 -->
## Version 0.3.5

Fix purchase confirmation overlay animation

### Changes

<!-- BEGIN CHANGES 0.3.5-->
- Fix purchase confirmation overlay animation
<!-- END CHANGES 0.3.5 -->

<!-- END RELEASE v0.3.5 -->

<!-- BEGIN RELEASE v0.3.4 -->
## Version 0.3.4

Purchase confirmation overlay

### Changes

<!-- BEGIN CHANGES 0.3.4-->
- Show and fade an overlay after completing a purchase or deposit
<!-- END CHANGES 0.3.4 -->

<!-- END RELEASE v0.3.4 -->

<!-- BEGIN RELEASE v0.3.3 -->
## Version 0.3.3

Multiple theme search paths

### Changes

<!-- BEGIN CHANGES 0.3.3-->
- Support for multiple theme search paths
<!-- END CHANGES 0.3.3 -->

<!-- END RELEASE v0.3.3 -->

<!-- BEGIN RELEASE v0.3.2 -->
## Version 0.3.2

Caching fix

### Changes

<!-- BEGIN CHANGES 0.3.2-->
- Cache busting for profile and product pictures
<!-- END CHANGES 0.3.2 -->

<!-- END RELEASE v0.3.2 -->

<!-- BEGIN RELEASE v0.3.1 -->
## Version 0.3.1

Package fix

### Changes

<!-- BEGIN CHANGES 0.3.1-->
- Fix version in deb package
<!-- END CHANGES 0.3.1 -->

<!-- END RELEASE v0.3.1 -->

<!-- BEGIN RELEASE v0.3.0 -->
## Version 0.3.0

THEMES!

### Changes

<!-- BEGIN CHANGES 0.3.0 -->
- Add support for theming
- Themes can override both templates and static files
<!-- END CHANGES 0.3.0 -->

<!-- END RELEASE v0.3.0 -->

<!-- BEGIN RELEASE v0.2.14 -->
## Version 0.2.14

UX release

### Changes

<!-- BEGIN CHANGES 0.2.14 -->
- Present an on-screen keyboard and disable file upload in kiosk mode (localhost)
<!-- END CHANGES 0.2.14 -->

<!-- END RELEASE v0.2.14 -->

<!-- BEGIN RELEASE v0.2.13 -->
## Version 0.2.13

UX release

### Changes

<!-- BEGIN CHANGES 0.2.13 -->
- Consistently sort users by name instead of user id
<!-- END CHANGES 0.2.13 -->

<!-- END RELEASE v0.2.13 -->

<!-- BEGIN RELEASE v0.2.12 -->
## Version 0.2.12

Bugfix release

### Changes

<!-- BEGIN CHANGES 0.2.12 -->
- Fix layout issues in transfer/deposit entry form
- Add missing email field in signup form
<!-- END CHANGES 0.2.12 -->

<!-- END RELEASE v0.2.12 -->

<!-- BEGIN RELEASE v0.2.11 -->
## Version 0.2.11

Feature release

### Changes

<!-- BEGIN CHANGES 0.2.11 -->
- Feature: Permit user signup
<!-- END CHANGES 0.2.11 -->

<!-- END RELEASE v0.2.11 -->

<!-- BEGIN RELEASE v0.2.10 -->
## Version 0.2.10

Feature release, Python 3.9

### Changes

<!-- BEGIN CHANGES 0.2.10 -->
- Use Python 3.9 by default
- Feature: Let users transfer funds to another account
<!-- END CHANGES 0.2.10 -->

<!-- END RELEASE v0.2.10 -->

<!-- BEGIN RELEASE v0.2.9 -->
## Version 0.2.9

Enhancement

### Changes

<!-- BEGIN CHANGES 0.2.9 -->
- Enhancement: Disable text selection and prevent image dragging for better touchscreen support
<!-- END CHANGES 0.2.9 -->

<!-- END RELEASE v0.2.9 -->

<!-- BEGIN RELEASE v0.2.8 -->
## Version 0.2.8

Feature release

### Changes

<!-- BEGIN CHANGES 0.2.8 -->
- Feature: Add "custom price" products
- Fix: Buying not working when using the NullDispenser
- Breaking: Remove Arch Linux packaging
<!-- END CHANGES 0.2.8 -->

<!-- END RELEASE v0.2.8 -->

<!-- BEGIN RELEASE v0.2.7 -->
## Version 0.2.7

Feature release

### Changes

<!-- BEGIN CHANGES 0.2.7 -->
- Feature: More touch-friendly deposit interface
<!-- END CHANGES 0.2.7 -->

<!-- END RELEASE v0.2.7 -->

<!-- BEGIN RELEASE v0.2.6 -->
## Version 0.2.6

Bugfix release

### Changes

<!-- BEGIN CHANGES 0.2.6 -->
- Fix: Improve support for stock providers
<!-- END CHANGES 0.2.6 -->

<!-- END RELEASE v0.2.6 -->

<!-- BEGIN RELEASE v0.2.5 -->
## Version 0.2.5

Feature release

### Changes

<!-- BEGIN CHANGES 0.2.5 -->
- Feature: Non-stockable products
- Feature: Pluggable stock provider and dispenser modules
- Fix: Products creation raised an error if no image was uploaded
<!-- END CHANGES 0.2.5 -->

<!-- END RELEASE v0.2.5 -->

<!-- BEGIN RELEASE v0.2.4 -->
## Version 0.2.4

Feature release

### Changes

<!-- BEGIN CHANGES 0.2.4 -->
- Feature: Quick-and-dirty Prometheus metrics exporter
<!-- END CHANGES 0.2.4 -->

<!-- END RELEASE v0.2.4 -->

<!-- BEGIN RELEASE v0.2.3 -->
## Version 0.2.3

Bugfix fix release

### Changes

<!-- BEGIN CHANGES 0.2.3 -->
- Fix: Session timeout lead to 500 error
<!-- END CHANGES 0.2.3 -->

<!-- END RELEASE v0.2.3 -->

<!-- BEGIN RELEASE v0.2.2 -->
## Version 0.2.2

Security fix release

### Changes

<!-- BEGIN CHANGES 0.2.2 -->
- Fix: Sessions were shared between clients
<!-- END CHANGES 0.2.2 -->

<!-- END RELEASE v0.2.2 -->

<!-- BEGIN RELEASE v0.2.1 -->
## Version 0.2.1

Hotfix release

### Changes

<!-- BEGIN CHANGES 0.2.1 -->
- Fix: Properly load config
<!-- END CHANGES 0.2.1 -->

<!-- END RELEASE v0.2.1 -->

<!-- BEGIN RELEASE v0.2 -->
## Version 0.2

Switch web framework, UI fixes.

### Changes

<!-- BEGIN CHANGES 0.2 -->
- Migrate from custom web framework to bottle.py
- Minor UI changes, more touchscreen-friendly
<!-- END CHANGES 0.2 -->

<!-- END RELEASE v0.2 -->

<!-- BEGIN RELEASE v0.1.1 -->
## Version 0.1.1

Minor bugfix release.

### Changes

<!-- BEGIN CHANGES 0.1.1 -->
- Fixed: 500 Error when attempting to change the password with a wrong old password.
<!-- END CHANGES 0.1.1 -->

<!-- END RELEASE v0.1.1 -->


<!-- BEGIN RELEASE v0.1 -->
## Version 0.1

First somewhat stable version of Matemat.

### Changes

<!-- BEGIN CHANGES 0.1 -->
- First somewhat stable version.
- Went back to Python 3.6 for Debian packaging.
- Automated deployment & release management.
<!-- END CHANGES 0.1 -->

<!-- END RELEASE v0.1 -->
