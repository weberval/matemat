from typing import Any, Dict

import os.path
import jinja2

from matemat import __version__
from matemat.util.currency_format import format_chf

__jinja_env: jinja2.Environment = None


def init(config: Dict[str, Any]) -> None:
    global __jinja_env
    themepath = os.path.abspath(os.path.join(config['themeroot'], config['theme'], 'templates'))
    __jinja_env = jinja2.Environment(
        loader=jinja2.FileSystemLoader([themepath, os.path.abspath(config['templateroot'])]),
        autoescape=jinja2.select_autoescape(default=True),
    )
    __jinja_env.filters['chf'] = format_chf


def render(name: str, **kwargs):
    global __jinja_env
    template: jinja2.Template = __jinja_env.get_template(name)
    return template.render(__version__=__version__, **kwargs).encode('utf-8')
