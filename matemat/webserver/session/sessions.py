from typing import Any, Dict, Tuple, Optional

from bottle import request, response
from secrets import token_bytes
from uuid import uuid4
from datetime import datetime, timedelta

__key: Optional[str] = token_bytes(32)

__session_vars: Dict[str, Tuple[datetime, Dict[str, Any]]] = dict()

# Inactivity timeout for client sessions
_SESSION_TIMEOUT: int = 3600
_COOKIE_NAME: str = 'matemat_session_id'


def start() -> str:
    """
    Start a new session, or resume the session identified by the session cookie sent in the HTTP request.

    :return: The session ID.
    """
    # Reference date for session timeout
    now = datetime.utcnow()
    # Read the client's session ID, if any
    session_id = request.get_cookie(_COOKIE_NAME, secret=__key)
    # If there is no active session, create a new session ID
    if session_id is None:
        session_id = str(uuid4())
    else:
        session_id = str(session_id)

    # Check for session timeout
    if session_id in __session_vars and __session_vars[session_id][0] < now:
        end(session_id)
        # Create new session ID after terminating the previous session
        session_id = str(uuid4())
    # Update or initialize the session timeout
    if session_id not in __session_vars:
        __session_vars[session_id] = (now + timedelta(seconds=_SESSION_TIMEOUT)), dict()
    else:
        __session_vars[session_id] = \
            (now + timedelta(seconds=_SESSION_TIMEOUT), __session_vars[session_id][1])
    # Return the session ID and timeout
    response.set_cookie(_COOKIE_NAME, session_id, secret=__key)
    return session_id


def end(session_id: str) -> None:
    """
    Destroy a session identified by the session ID.

    :param session_id: ID of the session to destroy.
    """
    if session_id in __session_vars:
        del __session_vars[session_id]


def put(session_id: str, key: str, value: Any) -> None:
    if session_id in __session_vars:
        __session_vars[session_id][1][key] = value


def get(session_id: str, key: str) -> Any:
    if session_id in __session_vars and key in __session_vars[session_id][1]:
        return __session_vars[session_id][1][key]
    return None


def delete(session_id: str, key: str) -> None:
    if session_id in __session_vars and key in __session_vars[session_id][1]:
        del __session_vars[session_id][1][key]


def has(session_id: str, key: str) -> bool:
    return session_id in __session_vars and key in __session_vars[session_id][1]
