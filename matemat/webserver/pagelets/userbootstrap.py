from bottle import get, post, redirect, abort, request

from matemat.db import MatematDatabase
from matemat.webserver import template
from matemat.webserver.config import get_app_config


@get('/userbootstrap')
@post('/userbootstrap')
def userbootstrap():
    """
    The page for creating a first admin user.  To be used when the system is set up the first time, or when there are no
    admin users left.
    """
    config = get_app_config()
    with MatematDatabase(config['DatabaseFile']) as db:
        # Redirect to main if there are still administrators registered
        if db.has_admin_users():
            redirect('/')
        # Process submission
        if request.method == 'POST':
            # Make sure all required values are present
            if 'username' not in request.params or 'password' not in request.params \
                    or 'password2' not in request.params:
                abort(400, 'Some arguments are missing')
            username: str = str(request.params.username)
            password: str = str(request.params.password)
            password2: str = str(request.params.password2)
            # The 2 passwords must match
            if password != password2:
                redirect('/userbootstrap')
            # Create the admin user
            db.create_user(username, password, None, True, False)
            # Redirect to the main page
            redirect('/')
        # Requested via GET; show the user creation UI
        else:
            return template.render('userbootstrap.html',
                                   setupname=config['InstanceName'])
