from bottle import abort, response, route

from matemat.db import MatematDatabase
from matemat.webserver.config import get_app_config


@route('/metrics')
def metrics():
    config = get_app_config()
    if config.get('MetricsEnabled', '0') != '1':
        return abort(403,
                     'Prometheus metrics are disable in configuration. '
                     'If you believe this is an error, contact your administrator.')
    with MatematDatabase(config['DatabaseFile']) as db:
        products = db.list_products()
    m = '''# TYPE matemat_beverage_supply_count gauge
# HELP matemat_beverage_supply_count Number of bottles available
'''
    for product in products:
        name = product.name.replace('"', '\\"')
        m += f'matemat_beverage_supply_count{{product="{name}"}} {product.stock}\n'
    response.headers['Content-Type'] = 'text/plain'
    return m
