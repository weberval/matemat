from bottle import get, post, redirect, request

from matemat.db import MatematDatabase
from matemat.webserver import session
from matemat.webserver import config as c


@get('/transfer')
@post('/transfer')
def transfer():
    """
    The transfer mechanism to tranfer funds between accounts.
    """
    config = c.get_app_config()
    session_id: str = session.start()
    # If no user is logged in, redirect to the main page, as a purchase must always be bound to a user
    if not session.has(session_id, 'authenticated_user'):
        redirect('/')
    # Connect to the database
    with MatematDatabase(config['DatabaseFile']) as db:
        # Fetch the authenticated user from the database
        uid: int = session.get(session_id, 'authenticated_user')
        user = db.get_user(uid)
        if 'target' not in request.params or 'n' not in request.params:
            redirect('/')
            return
        # Fetch the target user from the database
        tuid = int(str(request.params.target))
        transfer_user = db.get_user(tuid)
        # Read and transfer amount between accounts
        amount = int(str(request.params.n))
        db.transfer(user, transfer_user, amount)
    # Redirect to the main page (where this request should have come from)
    redirect('/')
