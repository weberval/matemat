
import os
from shutil import copyfile
from io import BytesIO

import magic
from bottle import get, post, redirect, abort, request, FormsDict
from PIL import Image
import netaddr

from matemat.db import MatematDatabase
from matemat.db.primitives import User
from matemat.webserver import template, session
from matemat.webserver.config import get_app_config


def signup_user(args: FormsDict, files: FormsDict, db: MatematDatabase) -> User:
    config = get_app_config()
    if 'username' not in args or 'password' not in args or 'password2' not in args:
        raise ValueError('Required fields missing')
    username = str(args.username)
    password = str(args.password)
    password2 = str(args.password2)
    if password != password2:
        raise ValueError('Passwords do not match')
    email = None
    if 'email' in args and len(args.email) > 0:
        email = str(args.email)

    new_user = db.create_user(username, password, email=email, admin=False, member=False)
    # If a touchkey was provided, set it
    if 'touchkey' in args and len(args.touchkey) > 0:
        touchkey = str(args.touchkey)
        db.change_touchkey(new_user, password, touchkey, verify_password=False)
    # Finally, set the avatar, if provided
    if 'avatar' in files:
        avatar = files.avatar.file.read()
        if len(avatar) > 0:
            filemagic: magic.FileMagic = magic.detect_from_content(avatar)
            if filemagic.mime_type.startswith('image/'):
                abspath: str = os.path.join(os.path.abspath(config['UploadDir']), 'thumbnails/users/')
                os.makedirs(abspath, exist_ok=True)
                # Parse the image data
                image: Image = Image.open(BytesIO(avatar))
                # Resize the image to 150x150
                image.thumbnail((150, 150), Image.LANCZOS)
                # Write the image to the file
                image.save(os.path.join(abspath, f'{new_user.id}.png'), 'PNG')
    else:
        # If a default avatar is set, copy it to the user's avatar path
        # Create the absolute path of the upload directory
        abspath: str = os.path.join(os.path.abspath(config['UploadDir']), 'thumbnails/users/')
        # Derive the individual paths
        default: str = os.path.join(abspath, 'default.png')
        userimg: str = os.path.join(abspath, f'{new_user.id}.png')
        # Copy the default image, if it exists
        if os.path.exists(default):
            copyfile(default, userimg, follow_symlinks=True)
    return new_user


@get('/signup')
@post('/signup')
def signup():
    """
    The password login mechanism.  If called via GET, render the UI template; if called via POST, attempt to log in with
    the provided credentials (username and passsword).
    """
    config = get_app_config()
    session_id: str = session.start()
    # If signup is not enabled, redirect to the main page
    if config.get('SignupEnabled', '0') != '1':
        redirect('/')
    # If a user is already logged in, simply redirect to the main page, showing the product list
    if session.has(session_id, 'authenticated_user'):
        redirect('/')
    # If requested via HTTP POST, read the request arguments and attempt to create a new user
    if request.method == 'POST':
        # Connect to the database and create the user
        with MatematDatabase(config['DatabaseFile']) as db:
            try:
                user = signup_user(request.params, request.files, db)
            except ValueError as e:
                redirect('/signup')
            # Set the user ID session variable
            session.put(session_id, 'authenticated_user', user.id)
            # Set the authlevel session variable (0 = none, 1 = touchkey, 2 = password login)
            session.put(session_id, 'authentication_level', 2)
            # Redirect to the main page, showing the product list
            redirect('/')
    elif request.method != 'GET':
        abort(405, 'Method not allowed')

    acl = netaddr.IPSet([addr.strip() for addr in config.get('SignupKioskMode', '').split(',')])
    if request.remote_addr in acl:
        return template.render('signup_kiosk.html',
                               zip=zip,
                               setupname=config['InstanceName'])
    return template.render('signup.html',
                           setupname=config['InstanceName'])
