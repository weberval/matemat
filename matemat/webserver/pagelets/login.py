from bottle import get, post, redirect, abort, request

from matemat.db import MatematDatabase
from matemat.db.primitives import User
from matemat.exceptions import AuthenticationError
from matemat.webserver import template, session
from matemat.webserver.config import get_app_config


@get('/login')
@post('/login')
def login_page():
    """
    The password login mechanism.  If called via GET, render the UI template; if called via POST, attempt to log in with
    the provided credentials (username and passsword).
    """
    config = get_app_config()
    session_id: str = session.start()
    # If a user is already logged in, simply redirect to the main page, showing the product list
    if session.has(session_id, 'authenticated_user'):
        redirect('/')
    # If requested via HTTP GET, render the login page showing the login UI
    if request.method == 'GET':
        return template.render('login.html',
                               setupname=config['InstanceName'])
    # If requested via HTTP POST, read the request arguments and attempt to log in with the provided credentials
    elif request.method == 'POST':
        # Connect to the database
        with MatematDatabase(config['DatabaseFile']) as db:
            try:
                # Read the request arguments and attempt to log in with them
                user: User = db.login(str(request.params.username), str(request.params.password))
            except AuthenticationError:
                # Reload the touchkey login page on failure
                redirect('/login')
        # Set the user ID session variable
        session.put(session_id, 'authenticated_user', user.id)
        # Set the authlevel session variable (0 = none, 1 = touchkey, 2 = password login)
        session.put(session_id, 'authentication_level', 2)
        # Redirect to the main page, showing the product list
        redirect('/')
    # If neither GET nor POST was used, show a 405 Method Not Allowed error page
    abort(405)
