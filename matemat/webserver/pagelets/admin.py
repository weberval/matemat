import os
from datetime import datetime
from io import BytesIO
from shutil import copyfile

import magic
from PIL import Image
from bottle import get, post, abort, redirect, request, FormsDict

from matemat.db import MatematDatabase
from matemat.db.primitives import User, ReceiptPreference
from matemat.exceptions import AuthenticationError, DatabaseConsistencyError
from matemat.util.currency_format import parse_chf
from matemat.webserver import session, template
from matemat.webserver.config import get_app_config, get_stock_provider


@get('/admin')
@post('/admin')
def admin():
    """
    The admin panel, shows a user's own settings.  Additionally, for administrators, settings to modify other users and
    products are shown.
    """
    config = get_app_config()
    session_id: str = session.start()
    # If no user is logged in, redirect to the login page
    if not session.has(session_id, 'authentication_level') or not session.has(session_id, 'authenticated_user'):
        redirect('/login')
    authlevel: int = session.get(session_id, 'authentication_level')
    uid: int = session.get(session_id, 'authenticated_user')
    # Show a 403 Forbidden error page if no user is logged in (0) or a user logged in via touchkey (1)
    if authlevel < 2:
        abort(403)

    # Connect to the database
    with MatematDatabase(config['DatabaseFile']) as db:
        # Fetch the authenticated user
        user = db.get_user(uid)
        # If the POST request contains a "change" parameter, delegate the change handling to the function below
        if request.method == 'POST' and 'change' in request.params:
            handle_change(request.params, request.files, user, db)
        # If the POST request contains an "adminchange" parameter, delegate the change handling to the function below
        elif request.method == 'POST' and 'adminchange' in request.params and user.is_admin:
            handle_admin_change(request.params, request.files, db)

        # Fetch all existing users and products from the database
        users = db.list_users()
        products = db.list_products()
        # Render the "Admin/Settings" page
        now = str(int(datetime.utcnow().timestamp()))
        return template.render('admin.html',
                               authuser=user, authlevel=authlevel, users=users, products=products,
                               receipt_preference_class=ReceiptPreference, now=now,
                               setupname=config['InstanceName'], config_smtp_enabled=config['SmtpSendReceipts'])


def handle_change(args: FormsDict, files: FormsDict, user: User, db: MatematDatabase) -> None:
    """
    Write the changes requested by a user for its own account to the database.

    :param args: The FormsDict object passed to the pagelet.
    :param user: The user to edit.
    :param db: The database facade where changes are written to.
    """
    config = get_app_config()
    try:
        # Read the type of change requested by the user, then switch over it
        change = str(args.change)

        # The user requested a modification of its general account information (username, email)
        if change == 'account':
            # Username and email must be set in the request arguments
            if 'username' not in args or 'email' not in args:
                return
            username = str(args.username)
            email = str(args.email)
            # An empty e-mail field should be interpreted as NULL
            if len(email) == 0:
                email = None
            try:
                receipt_pref = ReceiptPreference(int(str(args.receipt_pref)))
            except ValueError:
                return
            # Attempt to update username, e-mail and receipt preference
            try:
                db.change_user(user, agent=None, name=username, email=email, receipt_pref=receipt_pref)
            except DatabaseConsistencyError:
                return

        # The user requested a password change
        elif change == 'password':
            # The old password and 2x the new password must be present
            if 'oldpass' not in args or 'newpass' not in args or 'newpass2' not in args:
                return
            # Read the passwords from the request arguments
            oldpass = str(args.oldpass)
            newpass = str(args.newpass)
            newpass2 = str(args.newpass2)
            # The two instances of the new password must match
            if newpass != newpass2:
                raise ValueError('New passwords don\'t match')
            # Write the new password to the database
            try:
                db.change_password(user, oldpass, newpass)
            except AuthenticationError:
                raise ValueError('Old password doesn\'t match')

        # The user requested a touchkey change
        elif change == 'touchkey':
            # The touchkey must be present
            if 'touchkey' not in args:
                return
            # Read the touchkey from the request arguments
            touchkey = str(args.touchkey)
            # An empty touchkey field should set the touchkey to NULL (disable touchkey login)
            if len(touchkey) == 0:
                touchkey = None
            # Write the new touchkey to the database
            db.change_touchkey(user, '', touchkey, verify_password=False)

        # The user requested an avatar change
        elif change == 'avatar':
            # The new avatar field must be present
            if 'avatar' not in files:
                return
            # Read the raw image data from the request
            avatar = files.avatar.file.read()
            # Only process the image, if its size is more than zero.  Zero size means no new image was uploaded
            if len(avatar) == 0:
                return
            # Detect the MIME type
            filemagic: magic.FileMagic = magic.detect_from_content(avatar)
            if not filemagic.mime_type.startswith('image/'):
                return
            # Create the absolute path of the upload directory
            abspath: str = os.path.join(os.path.abspath(config['UploadDir']), 'thumbnails/users/')
            os.makedirs(abspath, exist_ok=True)
            try:
                # Parse the image data
                image: Image = Image.open(BytesIO(avatar))
                # Resize the image to 150x150
                image.thumbnail((150, 150), Image.LANCZOS)
                # Write the image to the file
                image.save(os.path.join(abspath, f'{user.id}.png'), 'PNG')
            except OSError:
                return

    except UnicodeDecodeError:
        raise ValueError('an argument not a string')


def handle_admin_change(args: FormsDict, files: FormsDict, db: MatematDatabase):
    """
    Write the changes requested by an admin for users of products.

    :param args: The RequestArguments object passed to the pagelet.
    :param db: The database facade where changes are written to.
    """
    config = get_app_config()
    try:
        # Read the type of change requested by the admin, then switch over it
        change = str(args.adminchange)

        # The user requested to create a new user
        if change == 'newuser':
            # Only create a new user if all required properties of the user are present in the request arguments
            if 'username' not in args or 'password' not in args:
                return
            # Read the properties from the request arguments
            username = str(args.username)
            email = None
            if 'email' in args and len(args.email) > 0:
                # An empty e-mail field should be interpreted as NULL
                email = str(args.email)
            password = str(args.password)
            is_member = 'ismember' in args
            is_admin = 'isadmin' in args
            # Create the user in the database
            newuser: User = db.create_user(username, password, email, member=is_member, admin=is_admin)

            # If a default avatar is set, copy it to the user's avatar path

            # Create the absolute path of the upload directory
            abspath: str = os.path.join(os.path.abspath(config['UploadDir']), 'thumbnails/users/')
            # Derive the individual paths
            default: str = os.path.join(abspath, 'default.png')
            userimg: str = os.path.join(abspath, f'{newuser.id}.png')
            # Copy the default image, if it exists
            if os.path.exists(default):
                copyfile(default, userimg, follow_symlinks=True)

        # The user requested to create a new product
        elif change == 'newproduct':
            # Only create a new product if all required properties of the product are present in the request arguments
            for key in ['name', 'pricemember', 'pricenonmember']:
                if key not in args:
                    return
            # Read the properties from the request arguments
            name = str(args.name)
            price_member = parse_chf(str(args.pricemember))
            price_non_member = parse_chf(str(args.pricenonmember))
            custom_price = 'custom_price' in args
            stockable = 'stockable' in args
            # Create the user in the database
            newproduct = db.create_product(name, price_member, price_non_member, custom_price, stockable)
            # If a new product image was uploaded, process it
            image = files.image.file.read() if 'image' in files else None
            if image is not None and len(image) > 0:
                # Detect the MIME type
                filemagic: magic.FileMagic = magic.detect_from_content(image)
                if not filemagic.mime_type.startswith('image/'):
                    return
                # Create the absolute path of the upload directory
                abspath: str = os.path.join(os.path.abspath(config['UploadDir']), 'thumbnails/products/')
                os.makedirs(abspath, exist_ok=True)
                try:
                    # Parse the image data
                    image: Image = Image.open(BytesIO(image))
                    # Resize the image to 150x150
                    image.thumbnail((150, 150), Image.LANCZOS)
                    # Write the image to the file
                    image.save(os.path.join(abspath, f'{newproduct.id}.png'), 'PNG')
                except OSError:
                    return
            else:
                # If no image was uploaded and a default avatar is set, copy it to the product's avatar path

                # Create the absolute path of the upload directory
                abspath: str = os.path.join(os.path.abspath(config['UploadDir']), 'thumbnails/products/')
                # Derive the individual paths
                default: str = os.path.join(abspath, 'default.png')
                userimg: str = os.path.join(abspath, f'{newproduct.id}.png')
                # Copy the default image, if it exists
                if os.path.exists(default):
                    copyfile(default, userimg, follow_symlinks=True)

        # The user requested to restock a product
        elif change == 'restock':
            stock_provider = get_stock_provider()
            if not stock_provider.needs_update():
                return
            # Only restock a product if all required properties are present in the request arguments
            if 'productid' not in args or 'amount' not in args:
                return
            # Read the properties from the request arguments
            productid = int(str(args.productid))
            amount = int(str(args.amount))
            # Fetch the product to restock from the database
            product = db.get_product(productid)
            if not product.stockable:
                return
            stock_provider.update_stock(product, amount)

        # The user requested to set default images
        elif change == 'defaultimg':
            # Iterate the possible images to set
            for category in 'users', 'products':
                if category not in files:
                    continue
                # Read the raw image data from the request
                default: bytes = files[category].file.read()
                # Only process the image, if its size is more than zero.  Zero size means no new image was uploaded
                if len(default) == 0:
                    continue
                # Detect the MIME type
                filemagic: magic.FileMagic = magic.detect_from_content(default)
                if not filemagic.mime_type.startswith('image/'):
                    continue
                # Create the absolute path of the upload directory
                abspath: str = os.path.join(os.path.abspath(config['UploadDir']), f'thumbnails/{category}/')
                os.makedirs(abspath, exist_ok=True)
                try:
                    # Parse the image data
                    image: Image = Image.open(BytesIO(default))
                    # Resize the image to 150x150
                    image.thumbnail((150, 150), Image.LANCZOS)
                    # Write the image to the file
                    image.save(os.path.join(abspath, f'default.png'), 'PNG')
                except OSError:
                    return

    except UnicodeDecodeError:
        raise ValueError('an argument not a string')
