from bottle import get, post, redirect, request

from matemat.db import MatematDatabase
from matemat.webserver import session
from matemat.webserver.config import get_app_config


@get('/deposit')
@post('/deposit')
def deposit():
    """
    The cash depositing mechanism.  Called by the user submitting a deposit from the product list.
    """
    config = get_app_config()
    session_id: str = session.start()
    # If no user is logged in, redirect to the main page, as a deposit must always be bound to a user
    if not session.has(session_id, 'authenticated_user'):
        redirect('/')
    # Connect to the database
    with MatematDatabase(config['DatabaseFile']) as db:
        # Fetch the authenticated user from the database
        uid: int = session.get(session_id, 'authenticated_user')
        user = db.get_user(uid)
        if 'n' in request.params:
            # Read the amount of cash to deposit from the request arguments
            n = int(str(request.params.n))
            # Write the deposit to the database
            db.deposit(user, n)
            # Redirect to the main page (where this request should have come from)
            redirect(f'/?lastaction=deposit&lastprice={n}')
    redirect('/')
