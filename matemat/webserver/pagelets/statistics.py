from datetime import datetime, timedelta
from math import pi, sin, cos
from typing import Any, Dict, List, Tuple

from bottle import route, redirect, abort, request

from matemat.db import MatematDatabase
from matemat.db.primitives import User
from matemat.webserver import template, session
from matemat.webserver.config import get_app_config


@route('/statistics')
def statistics():
    """
    The statistics page available from the admin panel.
    """
    config = get_app_config()
    session_id: str = session.start()
    # If no user is logged in, redirect to the login page
    if not session.has(session_id, 'authentication_level') or not session.has(session_id, 'authenticated_user'):
        redirect('/login')
    authlevel: int = session.get(session_id, 'authentication_level')
    auth_uid: int = session.get(session_id, 'authenticated_user')
    # Show a 403 Forbidden error page if no user is logged in (0) or a user logged in via touchkey (1)
    if authlevel < 2:
        abort(403)

    # Connect to the database
    with MatematDatabase(config['DatabaseFile']) as db:
        # Fetch the authenticated user
        authuser: User = db.get_user(auth_uid)
        if not authuser.is_admin:
            # Show a 403 Forbidden error page if the user is not an admin
            abort(403)

        todate: datetime = datetime.utcnow()
        fromdate: datetime = (todate - timedelta(days=365)).replace(hour=0, minute=0, second=0, microsecond=0)
        if 'fromdate' in request.params:
            fdarg: str = str(request.params.fromdate)
            fromdate = datetime.strptime(fdarg, '%Y-%m-%d').replace(tzinfo=None)
        if 'todate' in request.params:
            tdarg: str = str(request.params.todate)
            todate = datetime.strptime(tdarg, '%Y-%m-%d').replace(tzinfo=None)
        todate = (todate + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)

        stats: Dict[str, Any] = db.generate_sales_statistics(fromdate, todate)

        slices: List[Tuple[str, float, float, float, float, int, float, float, int]] = list()
        previous_angle: float = 0.0
        total = sum([i[1] for i in stats['consumptions'].values()])

        # Really hacky pie chart implementation
        for (product, (_, count)) in stats['consumptions'].items():
            angle: float = (count/total) * 2.0 * pi
            longarc: int = 0 if angle < pi else 1
            halfangle: float = angle / 2.0
            angle = (angle + previous_angle) % (2.0 * pi)
            halfangle = (halfangle + previous_angle) % (2.0 * pi)
            px: float = cos(previous_angle) * 100
            py: float = sin(previous_angle) * 100
            x: float = cos(angle) * 100
            y: float = sin(angle) * 100
            tx: float = cos(halfangle) * 130
            ty: float = sin(halfangle) * 130
            slices.append((product, px, py, x, y, longarc, tx, ty, count))
            previous_angle = angle

        # Render the statistics page
        return template.render('statistics.html',
                               fromdate=fromdate.strftime('%Y-%m-%d'),
                               todate=(todate - timedelta(days=1)).strftime('%Y-%m-%d'),
                               product_slices=slices,
                               authuser=authuser, authlevel=authlevel,
                               setupname=config['InstanceName'],
                               **stats)
