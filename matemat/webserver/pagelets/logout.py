from bottle import get, post, redirect

from matemat.webserver import session


@get('/logout')
@post('/logout')
def logout():
    """
    The logout mechanism, clearing the authentication values in the session storage.
    """
    session_id: str = session.start()
    # Remove the authenticated user ID from the session storage, if any
    if session.has(session_id, 'authenticated_user'):
        session.delete(session_id, 'authenticated_user')
    # Reset the authlevel session variable (0 = none, 1 = touchkey, 2 = password login)
    session.put(session_id, 'authentication_level', 0)
    # Redirect to the main page, showing the user list
    redirect('/')
