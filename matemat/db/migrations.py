
from typing import Dict

import sqlite3


def migrate_schema_1_to_2(c: sqlite3.Cursor):
    # Create missing tables
    c.execute('''
        CREATE TABLE transactions (
          ta_id INTEGER PRIMARY KEY,
          user_id INTEGER NOT NULL,
          value INTEGER(8) NOT NULL,
          old_balance INTEGER(8) NOT NULL,
          date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
          FOREIGN KEY (user_id) REFERENCES users(user_id)
            ON DELETE CASCADE ON UPDATE CASCADE
        );
    ''')
    c.execute('''
        CREATE TABLE consumptions (
          ta_id INTEGER PRIMARY KEY,
          product_id INTEGER DEFAULT NULL,
          FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
            ON DELETE CASCADE ON UPDATE CASCADE,
          FOREIGN KEY (product_id) REFERENCES products(product_id)
            ON DELETE SET NULL ON UPDATE CASCADE
        );
    ''')
    c.execute('''
        CREATE TABLE deposits (
          ta_id INTEGER PRIMARY KEY,
          FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
            ON DELETE CASCADE ON UPDATE CASCADE
        );
    ''')
    c.execute('''
        CREATE TABLE modifications (
          ta_id INTEGER NOT NULL,
          agent_id INTEGER NOT NULL,
          reason TEXT DEFAULT NULL,
          PRIMARY KEY (ta_id),
          FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
            ON DELETE CASCADE ON UPDATE CASCADE,
          FOREIGN KEY (agent_id) REFERENCES users(user_id)
            ON DELETE SET NULL ON UPDATE CASCADE
        );
    ''')

    #
    # Convert entries from the old consumption table into entries for the new consumptions table
    #

    # Fetch current users, their balance and membership status
    c.execute('SELECT user_id, balance, is_member FROM users')
    balances: Dict[int, int] = dict()
    memberships: Dict[int, bool] = dict()
    for user_id, balance, member in c:
        balances[user_id] = balance
        memberships[user_id] = bool(member)

    # Fetch current products and their prices
    c.execute('SELECT product_id, price_member, price_non_member FROM products')
    prices_member: Dict[int, int] = dict()
    prices_non_member: Dict[int, int] = dict()
    for product_id, price_member, price_non_member in c:
        prices_member[product_id] = price_member
        prices_non_member[product_id] = price_non_member

    # As the following migration does reverse insertions, compute the max. primary key that can occur, and
    # further down count downward from there
    c.execute('SELECT SUM(count) FROM consumption')
    ta_id: int = c.fetchone()[0]

    # Iterate (users x products)
    for user_id in balances.keys():
        member: bool = memberships[user_id]
        for product_id in prices_member:
            price: int = prices_member[product_id] if member else prices_non_member[product_id]

            # Select the number of items the user has bought from this product
            c.execute('''
                SELECT consumption.count FROM consumption
                  WHERE user_id = :user_id AND product_id = :product_id
                ''', {
                'user_id': user_id,
                'product_id': product_id
            })
            row = c.fetchone()
            if row is not None:
                count: int = row[0]
                # Insert one row per bought item, setting the date to NULL, as it is not known
                for _ in range(count):
                    # This migration "goes back in time", so after processing a purchase entry, "locally
                    # refund" the payment to reconstruct the "older" entries
                    balances[user_id] += price
                    # Insert into base table
                    c.execute('''
                        INSERT INTO transactions (ta_id, user_id, value, old_balance, date)
                          VALUES (:ta_id, :user_id, :value, :old_balance, NULL)
                        ''', {
                        'ta_id': ta_id,
                        'user_id': user_id,
                        'value': -price,
                        'old_balance': balances[user_id]
                    })
                    # Insert into specialization table
                    c.execute('INSERT INTO consumptions (ta_id, product_id) VALUES (:ta_id, :product_id)', {
                        'ta_id': ta_id,
                        'product_id': product_id
                    })
                    # Decrement the transaction table insertion primary key
                    ta_id -= 1
    # Drop the old consumption table
    c.execute('DROP TABLE consumption')


def migrate_schema_2_to_3(c: sqlite3.Cursor):
    # Add missing columns to users table
    c.execute('ALTER TABLE users ADD COLUMN receipt_pref INTEGER(1) NOT NULL DEFAULT 0')
    c.execute('''ALTER TABLE users ADD COLUMN created INTEGER(8) NOT NULL DEFAULT 0''')
    # Guess creation date based on the oldest entry in the database related to the user ( -1 minute for good measure)
    c.execute('''
    UPDATE users
      SET created = COALESCE(
        (SELECT MIN(t.date)
          FROM transactions AS t
          WHERE t.user_id = users.user_id),
        lastchange) - 60
    ''')

    # Fix ON DELETE in transactions table
    c.execute('''
    CREATE TABLE transactions_new (
      ta_id INTEGER PRIMARY KEY,
      user_id INTEGER DEFAULT NULL,
      value INTEGER(8) NOT NULL,
      old_balance INTEGER(8) NOT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    )
    ''')
    c.execute('INSERT INTO transactions_new SELECT * FROM transactions')
    c.execute('DROP TABLE transactions')
    c.execute('ALTER TABLE transactions_new RENAME TO transactions')

    # Change consumptions table
    c.execute('''
        CREATE TABLE consumptions_new (
          ta_id INTEGER PRIMARY KEY,
          product TEXT NOT NULL,
          FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
            ON DELETE CASCADE ON UPDATE CASCADE
        )
        ''')
    c.execute('''
        INSERT INTO consumptions_new (ta_id, product)
          SELECT c.ta_id, COALESCE(p.name, '<unknown>')
            FROM consumptions as c
            LEFT JOIN products as p
              ON c.product_id = p.product_id
        ''')
    c.execute('DROP TABLE consumptions')
    c.execute('ALTER TABLE consumptions_new RENAME TO consumptions')

    # Change modifications table
    c.execute('''
    CREATE TABLE modifications_new (
      ta_id INTEGER NOT NULL,
      agent TEXT NOT NULL,
      reason TEXT DEFAULT NULL,
      PRIMARY KEY (ta_id),
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    )
    ''')
    c.execute('''
    INSERT INTO modifications_new (ta_id, agent, reason)
      SELECT m.ta_id, COALESCE(u.username, '<unknown>'), m.reason
        FROM modifications as m
        LEFT JOIN users as u
          ON u.user_id = m.agent_id
    ''')
    c.execute('DROP TABLE modifications')
    c.execute('ALTER TABLE modifications_new RENAME TO modifications')

    # Create missing table
    c.execute('''
    CREATE TABLE receipts (  -- receipts sent to the users
      receipt_id INTEGER PRIMARY KEY,
      user_id INTEGER NOT NULL,
      first_ta_id INTEGER NOT NULL,
      last_ta_id INTEGER NOT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (first_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE,
      FOREIGN KEY (last_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    )
    ''')


def migrate_schema_3_to_4(c: sqlite3.Cursor):
    # Change receipts schema to allow null for transaction IDs
    c.execute('''
    CREATE TEMPORARY TABLE receipts_temp (
      receipt_id INTEGER PRIMARY KEY,
      user_id INTEGER NOT NULL,
      first_ta_id INTEGER DEFAULT NULL,
      last_ta_id INTEGER DEFAULT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (first_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE,
      FOREIGN KEY (last_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''')
    c.execute('INSERT INTO receipts_temp SELECT * FROM receipts')
    c.execute('DROP TABLE receipts')
    c.execute('''
    CREATE TABLE receipts (
      receipt_id INTEGER PRIMARY KEY,
      user_id INTEGER NOT NULL,
      first_ta_id INTEGER DEFAULT NULL,
      last_ta_id INTEGER DEFAULT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (first_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE,
      FOREIGN KEY (last_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''')
    c.execute('INSERT INTO receipts SELECT * FROM receipts_temp')
    c.execute('DROP TABLE receipts_temp')


def migrate_schema_4_to_5(c: sqlite3.Cursor):
    # Change products schema to allow null for stock and add stockable column
    c.execute('''
    CREATE TEMPORARY TABLE products_temp (
      product_id INTEGER PRIMARY KEY,
      name TEXT UNIQUE NOT NULL,
      stock INTEGER(8) NOT NULL DEFAULT 0,
      price_member INTEGER(8) NOT NULL,
      price_non_member INTEGER(8) NOT NULL
    );
    ''')
    c.execute('INSERT INTO products_temp SELECT * FROM products')
    c.execute('DROP TABLE products')
    c.execute('''
    CREATE TABLE products (
      product_id INTEGER PRIMARY KEY,
      name TEXT UNIQUE NOT NULL,
      stock INTEGER(8) DEFAULT 0,
      stockable INTEGER(1) DEFAULT 1,
      price_member INTEGER(8) NOT NULL,
      price_non_member INTEGER(8) NOT NULL
    );
    ''')
    c.execute('''
    INSERT INTO products SELECT product_id, name, stock, 1, price_member, price_non_member FROM products_temp
    ''')
    c.execute('DROP TABLE products_temp')


def migrate_schema_5_to_6(c: sqlite3.Cursor):
    # Add custom_price column
    c.execute('''
    ALTER TABLE products ADD COLUMN
    custom_price INTEGER(1) DEFAULT 0;
    ''')
