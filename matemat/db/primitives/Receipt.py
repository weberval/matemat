
from typing import List

from datetime import datetime

from matemat.db.primitives import User, Transaction


class Receipt:
    """
    Representation of a receipt for a user and a given timespan.

    :param _id: The receipt ID in the database.
    :param transactions: The list of transactions on this receipt.
    :param user: The user for whom this receipt was issued.
    :param from_date: The beginning of the time span this receipt covers.
    :param to_date: The end of the time span this receipt covers.
    """

    def __init__(self,
                 _id: int,
                 transactions: List[Transaction],
                 user: User,
                 from_date: datetime,
                 to_date: datetime) -> None:
        self.id: int = _id
        self.transactions: List[Transaction] = transactions
        self.user: User = user
        self.from_date: datetime = from_date
        self.to_date: datetime = to_date

    def __eq__(self, other) -> bool:
        if not isinstance(other, Receipt):
            return False
        return self.id == other.id and \
            self.transactions == other.transactions and \
            self.user == other.user and \
            self.from_date == other.from_date and \
            self.to_date == other.to_date

    def __hash__(self) -> int:
        return hash((self.id, self.transactions, self.user, self.from_date, self.to_date))
