
class Product:
    """
    Representation of a product offered by the Matemat, with a name, prices for users, and the number of items
    currently in stock.

    :param _id: The product ID in the database.
    :param name: The product's name.
    :param price_member: The price of a unit of this product for users marked as "members".
    :param price_non_member: The price of a unit of this product for users NOT marked as "members".
    :param custom_price: If true, the user can choose the price to pay, but at least the regular price.
    :param stock: The number of items of this product currently in stock, or None if not stockable.
    :param stockable: Whether this product is stockable.
    """

    def __init__(self, _id: int, name: str,
                 price_member: int, price_non_member: int, custom_price: bool,
                 stockable: bool, stock: int) -> None:
        self.id: int = _id
        self.name: str = name
        self.price_member: int = price_member
        self.price_non_member: int = price_non_member
        self.custom_price: bool = custom_price
        self.stock: int = stock
        self.stockable: bool = stockable

    def __eq__(self, other) -> bool:
        if not isinstance(other, Product):
            return False
        return self.id == other.id and \
            self.name == other.name and \
            self.price_member == other.price_member and \
            self.price_non_member == other.price_non_member and \
            self.custom_price == other.custom_price and \
            self.stock == other.stock and \
            self.stockable == other.stockable

    def __hash__(self) -> int:
        return hash((self.id, self.name, self.price_member, self.price_non_member, self.custom_price,
                     self.stock, self.stockable))
