from typing import Optional

from matemat.db import MatematDatabase
from matemat.db.primitives.Product import Product
from matemat.interfacing.stock import StockProvider
from matemat.webserver.config import get_app_config


class DatabaseStockProvider(StockProvider):

    @classmethod
    def needs_update(cls) -> bool:
        return True

    def get_stock(self, product: Product) -> Optional[int]:
        if not product.stockable:
            return None
        return product.stock

    def set_stock(self, product: Product, stock: int) -> None:
        if product.stock is None or not product.stockable:
            return
        config = get_app_config()
        with MatematDatabase(config['DatabaseFile']) as db:
            db.change_product(product, stock=stock)

    def update_stock(self, product: Product, stock: int) -> None:
        if product.stock is None or not product.stockable:
            return
        config = get_app_config()
        with MatematDatabase(config['DatabaseFile']) as db:
            db.change_product(product, stock=product.stock + stock)
