
from typing import Optional


class AuthenticationError(Exception):

    def __init__(self, msg: Optional[str] = None) -> None:
        super().__init__()
        self._msg: Optional[str] = msg

    def __str__(self) -> str:
        return f'AuthenticationError: {self._msg}'

    @property
    def msg(self) -> Optional[str]:
        return self._msg
