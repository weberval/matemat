"""
This package provides custom exception classes used in the Matemat codebase.
"""

from .AuthenticatonError import AuthenticationError
from .DatabaseConsistencyError import DatabaseConsistencyError
from .HttpException import HttpException
