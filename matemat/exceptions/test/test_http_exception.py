
import unittest

from matemat.exceptions import HttpException


class TestHttpException(unittest.TestCase):

    def test_all_args(self):
        e = HttpException(1337, 'Foo Bar', 'Lorem Ipsum Dolor Sit Amet')
        self.assertEqual(1337, e.status)
        self.assertEqual('Foo Bar', e.title)
        self.assertEqual('Lorem Ipsum Dolor Sit Amet', e.message)

    def test_default_args(self):
        e = HttpException()
        self.assertEqual(500, e.status)
        self.assertEqual('An error occurred', e.title)
        self.assertIsNone(e.message)
