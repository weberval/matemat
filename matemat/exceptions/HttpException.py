
from typing import Optional


class HttpException(Exception):

    def __init__(self, status: int = 500, title: str = 'An error occurred', message: str = None) -> None:
        super().__init__()
        self.__status: int = status
        self.__title: str = title
        self.__message: Optional[str] = message

    @property
    def status(self) -> int:
        return self.__status

    @property
    def title(self) -> str:
        return self.__title

    @property
    def message(self) -> Optional[str]:
        return self.__message
