
setTimeout(() => {
    let overlay = document.getElementById('overlay');
    if (overlay !== null) {
        overlay.style.display = 'block';
        setTimeout(() => {
            overlay.classList.add('fade');
            setTimeout(() => {
                setTimeout(() => {
                    overlay.classList.remove('fade');
                    setTimeout(() => {
                        overlay.style.display = 'none';
                    }, 700);
                }, 700);
            }, 700);
        }, 10);
    }
}, 0);